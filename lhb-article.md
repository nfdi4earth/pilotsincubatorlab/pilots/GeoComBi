---
name: GeoComBi
# The title of the article.

description: 
# A brief summary of the article, max. 300 characters incl. spaces.
Image and video data interpretation and annotation has become a bottleneck problem in
many fields of earth sciences due to the advances in imaging techniques, camera platforms
and the ongoing increase of volume recorded. Online image and video annotation tools such
as BIIGLE 2.0 (> 2,100 users, March 2023) made the organization, sharing, inspection and
semantic annotation of imagedata more efficient - for human interpretation and for the
generation of training data for AI systems. A current limitation of the inspection process
represents the lack of geospatial context as users interpret the visual data only using the
pixel information, neglecting the geospatial dimension and background. In this pilot, we will
implement two new BIIGLE 2.0 functions to provide geospatial context information in the
images/video annotation. First, a new geospatial browsing visualization will be implemented
to display the positions of images/video frames and the related annotations. Second, a new
geospatial context fusion function will combine a single image with visual spatial context
information (e.g. a mosaic) so users can gain and use a mental model of the surrounding
area. The two functions will be available for all BIIGLE 2.0 users, and like the BIIGLE 2.0
source code, publicly available under GPL 3.0 license. The BIIGLE 2.0 user community
ranges from Biology/Biodiversity research over Environmental Sciences to Earth Sciences.
Extending BIIGLE 2.0 with the functions we propose here will make the interpretation and
analysis of the visual data more efficient and allows users to integrate more geospatial
information in the image interpretation. This will lead to a more intense use of BIIGLE 2.0 by
a larger community which could have a positive effect on streamlining image analysis and
the definition of best practices in image analysis workflows.

author: 
  - name: Tim W. Nattkemper
    orcidid: https://orcid.org/0000-0002-7986-1158
  - name: Martin Zurowietz
    orcidid: https://orcid.org/0000-0002-7122-2343
  - name: Max Tiessen
    orcidid: https://orcid.org/0009-0008-5534-0578
#toDo: replace the authors with the project coworkers, the orcid is optional for all further author but mandatory for the first author.
# The author(s) of the article. The name must be given, the ORCID should be given. As many authors as necessary can be listed. 


language: ENG
# The language of the article. Accepted values are:  
# ENG for English
# DEU for German

collection: 
  - N4E_Pilots.md
# The filenames of the NFDI4Earth Living Handbook collection(s), the article belongs to, as they appear in https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs. 
# Alternatively, the titles of the collections. The editor will replace it with the collections' file names. 

subtype: article
# The type of the article. Acceptable terms are: 
# article: The most generic category. Should only be used if none of the others fit. 
# best_practice: A procedure of how a defined problem described in the article can be solved which is a (de-facto) standard or widely accepted in the community
# workflow: A step-by-step description of a procedure
# technical_note: A short article presenting a new software tool, method, technology. 
# recommended_article: a short review of a publication.
# discussion/review: An article that summarises/compares or (critically) discusses e.g., the state of understanding, different approaches or methodologies of a topic. 
# user_guide: Manuals on how to use e.g., software products. 
# collection: A NFDI4Earth Living Handbook collection article.
# FAQ: Can only used by the NFDI4Earth User Support Network, which define the FAQs. 

subject: 
  - dfgfo:313-02
  - unesco:concept160
  - unesco:mt2.35
# A list of controlled terms describing the subject of the article, given as their identifiers. Acceptable are any terms from: 
# DFG subject ontology: https://terminology.tib.eu/ts/ontologies/dfgfo/terms
# UNESCO thesaurus: https://vocabularies.unesco.org/browser/thesaurus/en/

keywords:
  - marine biology
  - geospatial context enrichment
  - visual data interpretation
# A list of terms to describe the article's topic more precisely. 

target_role: 
  - data collector
  - data user
  - data provider
  - research software engineer
# A list of the main roles of persons within research data management that is addressed by the article. 
# data collector: The person responsible for collecting, generating, re-using or contributing to research data and documents contexts and processes that relates to data handling. 
# data owner: Typically the principal researcher of the project or research group leader who is responsible for establishing who has access to the data, and reducing or mitigating the risk of any mishaps; e.g. improper storage facility, non-compliance with specific guidelines. 
# data depositor: Either the data creator or data owner and is the person who will perform the deposit of the data to a data preservation facility, like a repository or data archive. They perform activities such as uploading data after it has been generated, making it suitable for archiving, creating metadata and data documentation and submitting the data to the facility, so it can be preserved for the long-term. 
# data user: Someone who is interested in accessing and interacting with data for verification or (re)using data to evaluate, analyse, model and generally handle then to create insights and knowledge. access to research data for re-use or verification. 
# data curator: A person who ensures availability and reliability of data. They review and harmonise metadata of datasets submitted for storage and ensure that they are generally compliant with relevant policies before publication. They further monitor access to the data, support data owners in fulfilling data access requests and possess deeper technical knowledge about the backend of the repository or storage system. 
# data librarian: An information professional responsible for developing technical expertise on practical solutions of data management, archiving and dissemination and on data mining and visualization. 
# data steward: A supporter who advises researchers (or otherwise involved) persons on data management, privacy and information security aspects and can understand the meaning and applications of the data. They support: data creators on quality assurance, storage, policy compliance; data depositors on data selection, metadata and data documentation creation, preserving and publishing; data custodians on data curation, access controls, FAIR data management 
# data advocate: A person promoting the proper use of research data and technology and providing assistance or guidance in doing so. 
# data provider: A person or organization that stores and provides datasets to others.
# service provider: An organization that provides services related to the processing and management of data, such as, high-performance computing facilities or web-based maps.
# research software engineer: A person working with researchers to gain an understanding of the problems they face, and then develops, maintains and extends software to provide the answers. 
# policy maker: In research data management, policy makers strive to design effective, technology-neutral, forward-looking and coherent data governance policies across sectors.
# general public: All persons that do not belong to any of the other groups. 

identifier: 
# If the article has already a unique identifier such as an DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345). 

version: 0.1
# The version of the article; will be updated by the editor. 
---

# GeoComBi - Geospatial context enriched image annotation in BIIGLE 2.0

## Introduction
In the last 20 years, digital imaging and video (in the remainder we will use
the term “visual data” that relates to digital images and video) has evolved due to several
technical advances. Nowadays, large areas and habitats are inspected with remotely
controlled vehicles (e.g. ROV (remotely operated vehicle) or UAV (Unmanned Aerial
Vehicle)), autonomous vehicles (e.g. AUV (Autonomous Underwater Vehicle)) or fixed
observatory platforms and terabytes of visual data can be collected in a single exploration or
monitoring campaign. The analysis of the visual data includes different kinds of semantic
interpretation and annotation steps like assigning the whole image or a region of interest to a
semantic category, which could be a habitat category, an event category, a species descrip-
tion/morphotype/taxonomy or something else. This process is time-consuming and requires
domain experience. As the recorded data keeps growing in volume and resolution and the
number of expert time for interpretation is limited, a serious bottleneck situation has arisen.
To make the visual data analysis more efficient and effective, online image/video annotation
tools have been proposed that allow users to share data, annotate data with standardized
catalogs, visualize results and export the results as spreadsheets for subsequent statistical
analysis. Among the most prominent tools that represent the state of the art in this area are
VIAME, CoralNet and [BIIGLE 2.0](https://biigle.de "www.biigle.de") [@langenkamper2017biigle; @zurowietz2021current], 
which has been developed by the Biodata Mining Group at Bielefeld University in Germany (see Figure 1).

![Figure 1](./resources/images/biigle-features.svg "The online image and video annotation tool BIIGLE 2.0 supports image / video analysis tasks, like detecting / marking objects, taxonomic assignment / object classification (upper center and left), development / standardization of catalogs (upper right) and quality assessment / inspection (lower center). Examples show deep sea benthos, and aerial images from the Galapagos (copyright Dr. Amy MacLeod, “Iguanas from above” project). Numbers in the lower right are from March 2023.")

However, although the visual inspection and semantic annotation 
is now supported by such tools and some of them even offer machine learning modules to
automate some steps in the annotation, the tools do not support an integrative analysis of
geospatial information and visual data.
In the proposed pilot project we want to develop new BIIGLE 2.0 functions that allow users
to include geospatial context information in the interpretation of visual data. Linking the
image interpretation step to information about the large scale profile of the landscape / sea
floor has the potential to accelerate the analysis by geospatial filtering, to support an early
interpretation of the geospatial layout of annotated species or to support the decision in
object classification by integrating morphological visual features and geospatial features
displayed. In summary, an integrative multimodal (visual + geospatial data) approach should
have several positive effects on the depth and significance in visual data interpretation by
enriching geospatial information with information derived from visual data.

## Pilot description
BIIGLE 2.0 is a web application written in the PHP and JavaScript
languages with the Laravel and Vue.js application frameworks. A deployment of the software
is based on an Nginx webserver, PHP-FPM application server and PostgreSQL database as
Docker containers managed by Docker Compose. In this pilot, we will implement the two
following new functions to make use of geospatial information in BIIGLE 2.0. Prerequisite is
that GPS coordinates are available for the visual data and the geospatial map is available as
a georeferenced two-dimensional grid (e.g. a backscatter map, a bathymetry or a photo
mosaic) through a web map service (e.g. WMS). The two new functions will be implemented
following BIIGLE’s general modular design.

#### Geospatial browsing and filtering
Image collections from one dive (transect) or one UAV
flight are organized in so called volumes in BIIGLE. The proposed pilot project will support
the users to link one volume to one (or more) maps. Geospatial coordinates of images are
usually present in the file headers of the images or via metadata files such as iFDOs (image
FAIR (Findable, Accessible, Interoperable, Re-usable) Digital Objects [@schoening2022making]). 
Using BIIGLE’s filter tab allows users to display the map together for instance with
the positions of images in the volume (see Fig. 2 left). This map display can be used to
display all images available so that users can select a subset condition for geospatial
features or to select a subset of images after the annotation (for instance all images showing
a particular species) and visualize the positions of these images in the map. We will
investigate the potential of collaborating with other pilots such as “Bathy4All: Workflows for
Multibeam Processing and Visualization” or “German Marine Seismic Data Access”.

#### Geospatial context fusion display
A visualization function will be implemented to display
georeferenced grids as context information in the image annotation tool of BIIGLE. To this
end, we will develop a protocol for linking images provided with geospatial coordinates in a
volume to a web-accessible map, for instance a georeferenced photo-mosaic. In the image
annotation tool, the linked georeferenced grid(s) will then be displayed as surrounding
“background” information around the computed position of the current image and/or
annotations (cmp. Fig. 2).  

![Figure 2](./resources/images/projektantrag-features.png "Figure 2: Illustrations for the two geospatial visualization features developed in this pilot project. Left: Positions of images (i.e. the vehicle carrying the camera) are displayed on a bathymetric map so users can filter images condition to geospatial features or display subsets of images. Right: A single image from a drone flight image set is shown with a mosaic of all images in the background. The surrounding visual information guides users assessing to obtain geospatial context information.")

## References
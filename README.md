---
bibliography: bib.bib
---

# GeoComBi - Geospatial context enriched image annotation in BIIGLE 2.0

## Pilot description

[BIIGLE 2.0](https://biigle.de "www.biigle.de") [@langenkamper2017biigle; @zurowietz2021current] is a web application written in the PHP and JavaScript
languages with the Laravel and Vue.js application frameworks. A deployment of the software
is based on an Nginx webserver, PHP-FPM application server and PostgreSQL database as
Docker containers managed by Docker Compose. In this pilot, we will implement the two
following new functions to make use of geospatial information in BIIGLE 2.0. Prerequisite is
that GPS coordinates are available for the visual data and the geospatial map is available as
a georeferenced two-dimensional grid (e.g. a backscatter map, a bathymetry or a photo
mosaic) through a web map service (e.g. WMS). The two new functions will be implemented
following BIIGLE’s general modular design.

#### Geospatial browsing and filtering

Image collections from one dive (transect) or one UAV
flight are organized in so called volumes in BIIGLE. The proposed pilot project will support
the users to link one volume to one (or more) maps. Geospatial coordinates of images are
usually present in the file headers of the images or via metadata files such as iFDOs (image
FAIR (Findable, Accessible, Interoperable, Re-usable) Digital Objects [@schoening2022making]). 
Using BIIGLE’s filter tab allows users to display the map together for instance with
the positions of images in the volume (see Fig. 2 left). This map display can be used to
display all images available so that users can select a subset condition for geospatial
features or to select a subset of images after the annotation (for instance all images showing
a particular species) and visualize the positions of these images in the map. We will
investigate the potential of collaborating with other pilots such as “Bathy4All: Workflows for
Multibeam Processing and Visualization” or “German Marine Seismic Data Access”.

#### Geospatial context fusion display

A visualization function will be implemented to display
georeferenced grids as context information in the image annotation tool of BIIGLE. To this
end, we will develop a protocol for linking images provided with geospatial coordinates in a
volume to a web-accessible map, for instance a georeferenced photo-mosaic. In the image
annotation tool, the linked georeferenced grid(s) will then be displayed as surrounding
“background” information around the computed position of the current image and/or
annotations (cmp. Fig. 2).  

![Figure 2](./resources/images/projektantrag-features.png "Figure 2: Illustrations for the two geospatial visualization features developed in this pilot project. Left: Positions of images (i.e. the vehicle carrying the camera) are displayed on a bathymetric map so users can filter images condition to geospatial features or display subsets of images. Right: A single image from a drone flight image set is shown with a mosaic of all images in the background. The surrounding visual information guides users assessing to obtain geospatial context information.")

## References 

## Funding

This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).
